/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.discordchat;

import java.io.Serializable;

import javax.jms.JMSException;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PreDestroy;
import javax.annotation.PostConstruct;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author mimoun
 */
@ManagedBean
@SessionScoped
public class DiscordView implements Serializable {

    private static final long serialVersionUID = 3254181235309041386L;

    //Variables for JSF binding
    private boolean showInvite = false;
    private String message = "";
    private String channel = "";
    private String currentChannel = "<No channel selected>";

    //Main broker for the messaging application
    private DiscordApplicationBroker broker;

    /**
     * Setting up the DiscordApplicationBroker
     */
    @PostConstruct
    public void onload() {
        try {
            broker = new DiscordApplicationBroker();
        } catch (JMSException ex) {
            Logger.getLogger(DiscordView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Close all connections before closing down
     */
    @PreDestroy
    public void ondestroy() {
        try {
            broker.closeAll();
        } catch (JMSException ex) {
            Logger.getLogger(DiscordView.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Send a message to your current channel
     *
     * @throws JMSException
     * @throws InterruptedException
     */
    public void sendMessage() throws JMSException, InterruptedException {
        broker.sendMessage(currentChannel, message);
        this.message = "";
    }

    /**
     * Subscribe to a new channel
     */
    public void addChannel() {
        if (broker.topics.get(channel) == null) {
            try {
                broker.createNewChannel(channel);
                this.channel = "";
            } catch (JMSException ex) {
                Logger.getLogger(DiscordView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.print("Trying to create duplicate channel");
        }
    }

    /**
     * Open an existing channel
     *
     * @param channel
     */
    public void openChannel(String channel) {
        broker.refreshChatList(channel);
        currentChannel = channel;
        showInvite = true;
    }

    /**
     * Join an existing channel by invite
     *
     * @param channel
     */
    public void joinChannel(String channel) {
        if (broker.topics.get(channel) == null) {
            try {
                broker.createNewChannel(channel);
                this.channel = "";
            } catch (JMSException ex) {
                Logger.getLogger(DiscordView.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            System.out.print("Trying to create duplicate channel");
        }
    }

    /**
     * Invite all people to join your current channel
     *
     * @throws javax.jms.JMSException
     */
    public void invitePeople() throws JMSException {
        broker.inviteProducer.sendInvite(currentChannel);
    }

    /**
     * Accept the invite for a channel
     *
     * @param invite
     */
    public void acceptInvite(String invite) {
        joinChannel(invite);
        broker.invites.remove(invite);
    }

    /**
     * Decline invite for chatroom
     *
     * @param invite
     */
    public void declineInvite(String invite) {
        broker.invites.remove(invite);
    }

    /* Default getters and setters below */
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public DiscordApplicationBroker getBroker() {
        return broker;
    }

    public void setBroker(DiscordApplicationBroker broker) {
        this.broker = broker;
    }

    public String getCurrentChannel() {
        return currentChannel;
    }

    public void setCurrentChannel(String currentChannel) {
        this.currentChannel = currentChannel;
    }

    public Boolean getShowInvite() {
        return showInvite;
    }

    public void setShowInvite(Boolean showInvite) {
        this.showInvite = showInvite;
    }
}
